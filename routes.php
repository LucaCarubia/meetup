<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 12/02/18
 * Time: 22:38
 */

use Pecee\SimpleRouter\SimpleRouter;
require_once 'APIMiddleware.php';
require_once 'controllers/LoginController.php';
require_once 'controllers/DefaultController.php';
require_once 'controllers/SubscriberController.php';

// on ajoute un préfixe car le site se trouve dans mon cas à l'adresse :
// http://localhost/simplon/routeur/
$prefix = '/simplon/Projects/API';

SimpleRouter::group(['prefix' => $prefix], function () {
    SimpleRouter::match(['get', 'post'], '/login', 'LoginController@login')->name('login');
    SimpleRouter::get('/', 'DefaultController@defaultAction');
    SimpleRouter::get('/subscribers', 'SubscriberController@getAll')->addMiddleware(APIMiddleware::class);
    SimpleRouter::get('/subscriber/{id}', 'SubscriberController@getWithId')->addMiddleware(APIMiddleware::class);
});